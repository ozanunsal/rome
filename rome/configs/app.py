urls = {
    "cs9_manifest_url": (
        "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
        "main/package_list/cs9-image-manifest.lock.json"
    ),
    "dover_submit_endpoint": "http://dover:8080/submit/package_list",
}
fileName = "cs9-image-manifest.lock.json"
