import uuid

UUID_1 = str(uuid.uuid4())
UUID_PROPERTIES = {
    "durable": False,
    "auto_delete": True,
    "exclusive": True,
    "arguments": {},
}

QUEUES = {
    UUID_1: UUID_PROPERTIES,
}

BINDINGS = [
    {
        "exchange": "amq.topic",
        "queue": UUID_1,
        "routing_keys": [
            "org.centos.prod.buildsys.tag",
            "org.centos.prod.cbs.buildsys.tag",
        ],
    },
]
