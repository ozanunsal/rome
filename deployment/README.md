# Deployment

## Installation

- Install the required modules listed in the `deployment/requirements.txt`
```
pip install -r deployment/requirements.txt
```
- Download and install [OpenShift CLI](https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html)

## Prerequisite

- Login to the cluster
    - Using the `oc login` command retrieved from the openshift cluster 
## CI/CD

*rome* uses [OpenShift Pipelines](https://cloud.redhat.com/learn/topics/ci-cd) and [Tekton](https://tekton.dev/) as part of continuous integration and continuous delivery. The pipeline consists of tasks aiming to monitor the status of *lint* and *unit tests*. **rome-pipeline** integrates with [GitLab](https://gitlab.com/) through [webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html). For every new merge request, the pipeline will be triggered running. Once the pipeline's tasks finish running, the results will be sent back to [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

### To push the pipeline definition

**Prerequisite**

The following values must be provided in either `/deployment/vars/stage.yml` or `/deployment/vars/prod.yml` based on the environment.

```
ocp_host: #url of the OpenShift host
ocp_ns: #namespace into which k8s resources will be deployed
ocp_key: #API access token to the openshift host


group: #GitLab group
branch: #GitLab branch

gitlab_token: #GitLab API token
gitlab_key: #Any key that will identify the webhook (arbitrary)
project_id: #GitLab project ID
amqp_url_value: #url of RabbitMQ (example: amqp://guest:guest@rabbit-mq:5672/)
```

Navigate to the project root directory:
#### Start staging environment

```
make ci/stage/up
```

#### Start production environment

```
make ci/prod/up
```
## Auto deployment

Auto deployment automates the process of manually building and deploying the image to the cluster. The defined rules will take care of triggering the deployment process for every merge event on the **main** branch. This is as well done by integrating with [GitLab Webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)

### To push the deployment script

**Prerequisite**

The following values must be provided in either `/deployment/vars/stage.yml` or `/deployment/vars/prod.yml` based on the environment.

```
ocp_host: #url of the OpenShift host
ocp_ns: #namespace into which k8s resources will be deployed
ocp_key: #API access token to the openshift host


group: #GitLab group
branch: #GitLab branch

gitlab_token: #GitLab API token
gitlab_key: #Any key that will identify the webhook (arbitrary)
project_id: #GitLab project ID
amqp_url_value: #url of RabbitMQ (example: amqp://guest:guest@rabbit-mq:5672/)
```

Navigate to the project root directory:

#### Start staging environment

```
make stage/up
```

#### Start production environment

```
make prod/up
```
